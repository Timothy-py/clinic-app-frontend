var express =  require('express');
var cors = require('cors');
var path = require('path');
var ejsLayouts = require('express-ejs-layouts');
var moment = require("moment");

const app = express();
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'layouts/main');
app.use(ejsLayouts);

// app.set('port', (process.env.PORT || 8080));
app.set('port', (8082));

// INDEX PAGE
app.get('/', function(request, response) {
  response.send('Website working fine')
});

let todays_date = moment();

var patients = [
           {"id": 1,"firstName":"Bob","lastName":"Smith","gender":"Male", "ailment":"Malaria","health_status":"Improving", "date_admitted":moment("2020-06-01"), "expected_discharge_date":moment("2020-06-07")},
           {"id": 2,"firstName":"Timo","lastName":"Ade","gender":"Female", "ailment":"Headache","health_status":"Critical", "date_admitted":moment("2020-05-30"), "expected_discharge_date":moment("2020-06-17")},
           ];

// GET ALL PATIENTS
app.get('/clinic/patients', function(req, res) {
         
        res.render('pages/patient_list', { title: 'Patient List', patients: patients, layout: 'layouts/detail'} );
});

// HEALTH DASHBOARD
app.get('/clinic/patient/dashboard', function(req, res) {
        res.render('pages/dashboard', { title: 'Clinic Dashboard', patients: patients, todays_date: todays_date, layout: 'layouts/detail'} );
});

// GET ALL PATIENTS THAT ARE DUE FOR DISCHARGE
app.get('/clinic/patient/due', function(req, res) {
        res.render('pages/due_patient_list', { title: 'Due Patient List', patients: patients, todays_date: todays_date, layout: 'layouts/detail'} );
});

// GET ALL PATIENTS THAT ARE ADMITTED THIS MONTH
app.get('/clinic/patient/latest', function(req, res) {
        res.render('pages/latest_patient_list', { title: 'Latest Patient List', patients: patients, todays_date: todays_date, layout: 'layouts/detail'} );
});

// GET ALL PATIENTS WHOSE CONDITIONS ARE IMPROVING
app.get('/clinic/patient/improving', function(req, res) {
        res.render('pages/due_patient_list', { title: 'Improving Patient List', patients: patients, layout: 'layouts/detail'} );
});

// GET ONE PATIENT
app.get('/clinic/patient/:patient_id', function(req, res, next) {
        var patient = {"id": 1,"firstName":"Bob","lastName":"Smith","gender":"Male", "ailment":"Malaria","health_status":"Improving", "date_admitted":moment("2020-06-01"), "expected_discharge_date":moment("2020-06-07")};
        res.render('pages/patient_detail', { title: 'Patient Details', patient: patient, layout: 'layouts/detail'} );
});
 

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port')); 
});

